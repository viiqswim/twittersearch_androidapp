/**********************************************************
 * Title:
 * 		Twitter Search Application
 * Authors:
 * 		Victor Dozal, Dan Halloran
 * Class:
 * 		CSC 492, Mobile Computing
 * Instructors:
 * 		Dr. Logar
 * 		Professor Brian Butterfield
 * Date:
 * 		October 6th, 2013
 * Description:
 * 		This app allows users to save their favorite 
 * 		Twitter searches with easy-to-remember, user-chosen, 
 * 		short tag names. Users can then follow the tweets 
 * 		on their favorite topics.
 * Program Errors:
 * 		None found as of October 6th, 2013
 * 
 *********************************************************/

// Name of the package
package sdsmt.edu.halloran.app1;

// All the imported libraries
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collections;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;


/*********************************************************
 * Class definition. Extends Activity	
 * <p>
 * Contains the functions for the main flow 
 * 	of the app.
 * @author Victor Dozal, Dan Halloran
 * 
 ********************************************************/
public class MainActivity extends Activity {

	// Holds id of the search EditText field
	EditText searchEditText;
	// Holds the value of the Search EditText field
	String searchTextString;
	// Holds id of the search EditText field
	EditText tagEditText;
	// Holds the value of the Tag EditText field
	String tagTextString;
	
	// Holds the name of the preferences file
	String SAVED_SEARCHES_NAME = "tagPreferences";

	// Hold ids of the Save, Tag, Edit, 
	// Delete, and Clear buttons
	Button theSaveButton;
	Button theTagButton;
	Button theEditButton;
	Button theDeleteButton;
	Button theClearButton;
	
	// Holds the inflater
	LayoutInflater inflator;
	
	// Holds the tag row view
	View tagRowView;
	
	// Holds the table layout with the tag list
	TableLayout tagListTableLayout;
	// Contains the list of all the saved searches
	SharedPreferences savedSearches;
	
	// array to hold all the search tags
	String[] savedTagsArray;
	
	// Input to a function
	int index = 0;
	
	
	/************************************************************************
	 * 		onCreate()
	 * 		When the MainActivity is created, it renders everything.
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Bundle savedInstanceState
	 *  @return Nothing
	 ***********************************************************************/
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		savedSearches = getSharedPreferences(SAVED_SEARCHES_NAME, MODE_PRIVATE);
		// Retrieve stored tags from preferences
		savedTagsArray = savedSearches.getAll().keySet().toArray(new String[0]);
		// Sort tags.
		Arrays.sort(savedTagsArray, String.CASE_INSENSITIVE_ORDER);
		Arrays.sort(savedTagsArray, Collections.reverseOrder());
		
		// Assigns an Id to the tagListTableLayout
		tagListTableLayout = (TableLayout) findViewById(R.id.tagListTableLayout);
		
		for(int i = 0; i < savedTagsArray.length; i++){
			// Get reference to Layout Inflater in this context
			inflator = getLayoutInflater();
			tagRowView = inflator.inflate(R.layout.table_row, null);
			
			// Set Tag button text and register event listener
			theTagButton = (Button) tagRowView.findViewById(R.id.tagButton);
			theTagButton.setOnClickListener(tagButtonAction);
			theTagButton.setText(savedTagsArray[i]);
			theTagButton.setTag(savedTagsArray[i]);

			theEditButton = (Button) tagRowView.findViewById(R.id.button_edit);
			theEditButton.setOnClickListener(editButtonAction);
			// set a tag of the search tag to the edit button for later use
			theEditButton.setTag(savedTagsArray[i]);
			
			theDeleteButton = (Button) tagRowView.findViewById(R.id.button_delete);
			theDeleteButton.setOnClickListener(deleteButtonAction);
			// set tag of the search tag to the delete button for later use
			theDeleteButton.setTag(savedTagsArray[i]);
		
			// Add Tag and Edit buttons to the querytableLayout
			tagListTableLayout.addView(tagRowView, index);
		}
		
		
		// Assigns the corresponding ID to the Save and Clear buttons
		theSaveButton = (Button) findViewById(R.id.SaveButton);
		theClearButton = (Button) findViewById(R.id.clearTagsButton);
		// Assigns an action to the click of the buttons
		theSaveButton.setOnClickListener(saveButtonAction);
		theClearButton.setOnClickListener(clearButtonAction);
		
		
	}

	/************************************************************************
	 * 	onCreateOptionsMenu()
	 * 	Inflates the layout
	 * @author Victor Dozal, Dan Halloran
	 * @param Menu menu
	 * @return true/false
	 ***********************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/************************************************************************
	 * 	saveButtonAction()
	 * 	Saves a twitter search and tags in the application
	 * 
	 * @author Victor Dozal, Dan Halloran
	 * @param None
	 * @return None
	 ***********************************************************************/
	private OnClickListener saveButtonAction = new OnClickListener(){
		// This shizz is here to hide the isEmpty api error
		@SuppressLint("NewApi")
		@Override
		public void onClick(View v){
			// Find the EditText fields
			searchEditText = (EditText) findViewById(R.id.Search);
			tagEditText = (EditText) findViewById(R.id.Tag);
			// Convert EditText fields to strings
			searchTextString = searchEditText.getText().toString();
			tagTextString = tagEditText.getText().toString();
			
			if( !searchTextString.isEmpty() && !tagTextString.isEmpty()){
				// Retrieve any existing saved searches from user preferences
				savedSearches = getSharedPreferences(SAVED_SEARCHES_NAME, MODE_PRIVATE);
				

				
				// By tag, overwrite existing query or add new tag and query
				SharedPreferences.Editor preferenceEditor = savedSearches.edit();
				preferenceEditor.putString(tagTextString, searchTextString);
				preferenceEditor.apply();

				
				// Retrieve stored tags from preferences
				savedTagsArray = savedSearches.getAll().keySet().toArray(new String[0]);
				// Sort tags.
				Arrays.sort(savedTagsArray, String.CASE_INSENSITIVE_ORDER);
				Arrays.sort(savedTagsArray, Collections.reverseOrder());
				
				// Assigns an Id to the tagListTableLayout
				tagListTableLayout = (TableLayout) findViewById(R.id.tagListTableLayout);
				// Clears the tagView before re populating
				tagListTableLayout.removeAllViews();
				
				for(int i = 0; i < savedTagsArray.length; i++){
					// Get reference to Layout Inflater in this context
					inflator = getLayoutInflater();
					tagRowView = inflator.inflate(R.layout.table_row, null);
					
					// Set Tag button text and register event listener
					theTagButton = (Button) tagRowView.findViewById(R.id.tagButton);
					theTagButton.setOnClickListener(tagButtonAction);
					theTagButton.setText(savedTagsArray[i]);
					theTagButton.setTag(savedTagsArray[i]);

					theEditButton = (Button) tagRowView.findViewById(R.id.button_edit);
					theEditButton.setOnClickListener(editButtonAction);
					// set a tag of the search tag to the edit button for later use
					theEditButton.setTag(savedTagsArray[i]);
					
					theDeleteButton = (Button) tagRowView.findViewById(R.id.button_delete);
					theDeleteButton.setOnClickListener(deleteButtonAction);
					// set tag of the search tag to the delete button for later use
					theDeleteButton.setTag(savedTagsArray[i]);
				
					// Add Tag and Edit buttons to the querytableLayout
					tagListTableLayout.addView(tagRowView, index);
				}
				
				// Clear EditText fields
				searchEditText.setText("");
				tagEditText.setText("");
			}
		}
	};
	
	/************************************************************************
	 * 	tagButtonAction()
	 * 	When a Tag button is clicked, it sends the user to the browser
	 * 	and shows the saved search.
	 * 
	 * @author Victor Dozal, Dan Halloran
	 * @param None
	 * @return None
	 ***********************************************************************/
	private OnClickListener tagButtonAction = new OnClickListener(){
		@Override
		public void onClick(View v) {
			
			//Button tempTagButton = theTagButton;
			//String tempTag = tempTagButton.getText().toString();
			String tempTag = (String)v.getTag();
			String stringToSearch = savedSearches.getString(tempTag, "");
			
			// encode url to correct web url
			@SuppressWarnings("deprecation")
			String url = getString(R.string.searchURL) + URLEncoder.encode(stringToSearch);
			Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(webIntent);
		}
	};

	/************************************************************************
	 * 	editButtonAction()
	 * 	Allows the user to edit a Twitter Search
	 * 
	 * @author Victor Dozal, Dan Halloran
	 * @param None
	 * @return None
	 ***********************************************************************/
	private OnClickListener editButtonAction = new OnClickListener(){
		@TargetApi(Build.VERSION_CODES.GINGERBREAD)
		@Override
		public void onClick(View v) {
			// Retrieve the Tag of row
			//Button tempTagButton = theTagButton;
			String tempEditString = (String) v.getTag();
			
			// pull search value from shared preferences
			String stringToSearch = savedSearches.getString(tempEditString, "");
			
			// set text fields to correct string
			searchEditText.setText(stringToSearch);
			tagEditText.setText(tempEditString);
			
			SharedPreferences.Editor preferenceEditor = savedSearches.edit();
			// remove edited row
			preferenceEditor.remove(tempEditString);
			preferenceEditor.apply();
		}
		
	};
	
	/************************************************************************
	 * 	deleteButtonAction()
	 * 	Deletes a twitter search along with its tag. In essence,
	 * 	it deletes a whole row.
	 * 
	 * @author Victor Dozal, Dan Halloran
	 * @param None
	 * @return None
	 ***********************************************************************/
	private OnClickListener deleteButtonAction = new OnClickListener(){
		@TargetApi(Build.VERSION_CODES.GINGERBREAD)
		@Override
		public void onClick(View v) {
			// Holds a temporary value
			String tempEditString = (String) v.getTag();
			
			// Holds a list of currently saved preferences
			SharedPreferences.Editor preferenceEditor = savedSearches.edit();
			preferenceEditor.remove(tempEditString);
			preferenceEditor.apply();
			
			// Assigns an Id to the tagListTableLayout
			tagListTableLayout = (TableLayout) findViewById(R.id.tagListTableLayout);
			// Clears the tagView before re populating
			tagListTableLayout.removeAllViews();
			
			// save all shared preferences tags to the array
			savedTagsArray = savedSearches.getAll().keySet().toArray(new String[0]);
			// Sort tags.
			Arrays.sort(savedTagsArray, String.CASE_INSENSITIVE_ORDER);
			Arrays.sort(savedTagsArray, Collections.reverseOrder());
			
			for(int i = 0; i < savedTagsArray.length; i++){
				// Get reference to Layout Inflater in this context
				inflator = getLayoutInflater();
				tagRowView = inflator.inflate(R.layout.table_row, null);
				
				// Set Tag button text and register event listener
				theTagButton = (Button) tagRowView.findViewById(R.id.tagButton);
				theTagButton.setOnClickListener(tagButtonAction);
				theTagButton.setText(savedTagsArray[i]);

				theEditButton = (Button) tagRowView.findViewById(R.id.button_edit);
				theEditButton.setOnClickListener(editButtonAction);
				theEditButton.setTag(savedTagsArray[i]);
				
				theDeleteButton = (Button) tagRowView.findViewById(R.id.button_delete);
				theDeleteButton.setOnClickListener(deleteButtonAction);
				theDeleteButton.setTag(savedTagsArray[i]);
			
				// Add Tag and Edit buttons to the querytableLayout
				tagListTableLayout.addView(tagRowView, index);
			}
		}
		
	};

	/************************************************************************
	 * 	clearButtonAction()
	 * 	Asks the user if they are sure they want to clear all tags.
	 * 
	 * @author Victor Dozal, Dan Halloran
	 * @param None
	 * @output None
	 ***********************************************************************/
	private OnClickListener clearButtonAction = new OnClickListener(){

		@Override
		public void onClick(View v) {
			// Alert the user with a dialog confirming that they want to clear the tags
			AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
			alertBuilder.setTitle(R.string.confirmTitle);
			alertBuilder.setMessage(R.string.confirmMessage);
			alertBuilder.setCancelable(true);
			alertBuilder.setPositiveButton(R.string.yes, deleteAllTagsAction);
			alertBuilder.setNegativeButton(R.string.cancel, null);
			
			// Show the alert
			AlertDialog alert = alertBuilder.create();
			alert.show();
			
		}
	};

	/************************************************************************
	 * 	deleteAllTagsAction()
	 * 	Clears all the Twitter searches along with their tags.
	 * 	In essence, it deletes all Twitter Search rows.
	 * 
	 * @author Victor Dozal, Dan Halloran
	 * @param None
	 * @output None
	 ***********************************************************************/
	private DialogInterface.OnClickListener deleteAllTagsAction = new DialogInterface.OnClickListener(){
		// This is here so that I can do .apply, otherwise it throws an error
		@SuppressLint("NewApi")
		@Override
		public void onClick(DialogInterface dialog, int which) {

			// Delete all the tags
			tagListTableLayout.removeAllViews();
		
				
			// By tag, overwrite existing query or add new tag and query
			SharedPreferences.Editor preferenceEditor = savedSearches.edit();
			preferenceEditor.clear();
			preferenceEditor.commit();
		}
	};
}
